package net.freenose.lucener

import java.io.{FileInputStream, InputStreamReader, BufferedReader, File}
import java.util.Date
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.util.Version
import org.apache.lucene.index.{IndexWriter, IndexWriterConfig}
import org.apache.lucene.index.IndexWriterConfig.OpenMode
import org.apache.lucene.document._

class IndexFiles(indexPath: String, docsPath: String) {
  private val docDir = new File(docsPath)
  private val indexDir = new File(indexPath)

  def run() { run(true) }

  def run(create: Boolean) {
    val dir = FSDirectory.open(indexDir)
    val analyzer = new StandardAnalyzer(Version.LUCENE_40)
    val iwc = new IndexWriterConfig(Version.LUCENE_40, analyzer)

    val start = new Date()

    if (create) {
      iwc.setOpenMode(OpenMode.CREATE)
    } else {
      iwc.setOpenMode(OpenMode.CREATE_OR_APPEND)
    }

    val writer = new IndexWriter(dir, iwc)

    try {
      indexDocs(writer, docDir)
    } finally {
      writer.close()
    }

    val end = new Date()
    println((end.getTime - start.getTime).toString + " total milliseconds")

  }

  private def indexDocs(writer: IndexWriter, file: File) {
    if (!file.canRead) return

    if (file.isDirectory) {
      for (f <- file.listFiles())
        indexDocs(writer, f)
    } else {
      val doc = new Document()

      println("indexing file: " + file.getPath)

      // add a path field, index the field but don't tokenize it
      doc.add(new StringField("path", file.getPath, Field.Store.YES))

      // add the last modified date, use a LongField that's indexed
      doc.add(new LongField("modified", file.lastModified, Field.Store.NO))

      val reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))

      try {
        // add the contents, specify a reader so the text is tokenized and indexed
        doc.add(new TextField("contents", reader))
      } finally {
        reader.close()
      }
    }
  }
}

object IndexFiles {
  val UsageString =
    """
      |java net.freenose.lucener.IndexFiles
      |  [-index INDEX_PATH] [-docs DOCS_PATH] [-update]
      |
      |This indexes the documents in DOCS_PATH, creating a Lucene index
      |in INDEX_PATH that can be searched with SearchFiles
    """.stripMargin


  def main(args: Array[String]) {
    val idx = new IndexFiles(Default.IndexDir, Default.DocsPath)
    idx.run()
  }
}


