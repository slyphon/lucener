package net.freenose.lucener

import com.frugalmechanic.optparse.OptParse
import java.io.{InputStreamReader, FileInputStream, BufferedReader, File}
import org.apache.lucene.index.{DirectoryReader, IndexReader}
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.util.Version
import org.apache.lucene.queryparser.classic.QueryParser
import java.util.Date

/*
"Usage:\tjava org.apache.lucene.demo.SearchFiles [-index dir] [-field f] [-repeat n] [-queries file] [-query string] [-raw] [-paging hitsPerPage]\n\nSee http://lucene.apache.org/java/4_0/demo.html for details.";
*/

class SearchFiles(val reader: IndexReader,
                  val searcher: IndexSearcher,
                  val analyzer: StandardAnalyzer,
                  val parser: QueryParser,
                  val input: BufferedReader) {

  def repl() {
    if (!isQueries && !isQuery) println("Enter query: ")

    val rawLine = if (isQuery) queryString else input.readLine()

    if (rawLine == null || rawLine.length == -1) return

    val line = rawLine.trim

    if (line.length == 0) return

    val query = parser.parse(line)
    println("Searching for: " + query.toString(field))

    if (repeat > 0) {
      val start = new Date()
      var count = 0
      while (count < repeat) {
        searcher.search(query, null, 100)
        count += 1
      }
      val end = new Date()
      println("Time: " + (end.getTime - start.getTime) + "ms")
    }

    if (isQuery) return
    repl()  // Let's do it again! TCO FTW!
  }


  private def isQueries = SearchFiles.options.queries.isDefined
  private def isQuery = SearchFiles.options.query.isDefined
  private def queryString = SearchFiles.options.query.value.get
  private def field = SearchFiles.options.field.value.get
  private def repeat = SearchFiles.options.repeat.value.get

}

object SearchFiles {
  object options extends OptParse {
    val index = FileOpt(
      default = new File(Default.IndexDir),
      desc = "index dir"
    )

    val field = StrOpt(default = "contents")

    val repeat = IntOpt(
      short = 'r',
      default = 0,
      desc = "dunno what it does, but it better be an int"
    )

    val queries = FileOpt(desc = "file")
    val query = StrOpt(desc = "string")
    val raw = BoolOpt(default = false)

    val paging = IntOpt(
      desc = "hitsPerPage, must be >1",
      validate = (i:Int) => (i > 0)
    )
  }

  def main(args: Array[String]) {
    options.parse(args)

    withIndexReader(options.index.get) { reader =>
      repl(reader)
    }

  }

  private def repl(reader: IndexReader) {
    val searcher = new IndexSearcher(reader)
    val analyzer = new StandardAnalyzer(Version.LUCENE_40)
    val parser = new QueryParser(Version.LUCENE_40, options.field.get, analyzer)

  }

  private def withBufferedIn(f: BufferedReader => Unit) {
    val in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"))
    try {
      f(in)
    } finally {
      in.close()
    }
  }

  private def inputStream: FileInputStream = {
    options.queries.value match {
      case Some(f)  => new FileInputStream(f)
      case None     => System.in
    }
  }

  private def withIndexReader(file: File)(f: IndexReader => Unit) {
    val reader = DirectoryReader.open(FSDirectory.open(file))
    try {
      f(reader)
    } finally {
      reader.close()
    }
  }
}
